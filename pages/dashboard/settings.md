---
title: Dashboard
---
<!--
This page currently only includes Stripe as a payment option, per production
status, but the payment-option class can be extended to handle other payment
options.
-->

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Dashboard</h1>
    {% include "dashboard-nav.html" %}
  </header>

  {# If user has set up the payment option, show activated state. #}
  {# The form ids and hidden fields are example markers only. #}
  {# Please refer to production for the correct form ids and values. #}
  {% if user.settings.stripe_active %}
    <form class="payment-option --active" method="post" id="hident4"
    enctype="application/x-www-form-urlencoded">
      <div class="__box --update">
        <div class="__status">active!</div>
        <button class="__button --update">update Stripe</button>
        <div class="__logo --stripe"></div>
      </div>
      <div class="__box --delete">
        <a class="__button --delete" href="#">
          Delete payment info and remove pledges
        </a>
      </div>
    </form>
  {% else %}
    <form class="payment-option" method="post" id="hident4"
    enctype="application/x-www-form-urlencoded">
      <input type="hidden" name="_formid" value="">
      <input type="hidden" name="_token" value="">
      <label for="hident3"></label>
      <input type="hidden" id="hident3" name="f1" value="">
      <div class="__status">not active</div>
      <div class="__box">
        <button class="__button --setup" id="hident5">set up Stripe</button>
        <div class="__logo --stripe"></div>
      </div>
    </form>
  {% endif %}

  <section class="payment-intro">
    <p class="__text">
      Snowdrift.coop does <em class="__em">not</em> hold or touch your credit
      card information. The entry form widget is run by and goes directly
      to <a class="__link" href="https://stripe.com/">Stripe</a>, which manages
      the money transfers.
    </p>
    <p class="__text">
      Use of this page (and this page alone on Snowdrift.coop) is governed by
      Stripe's <a class="__link" href="https://stripe.com/us/legal">terms of
      service</a>, since this page uses their proprietary JavaScript code. We
      hope to rectify this in the near future. :)
    </p>
  </section>
</main>
