---
title: Create Account
---

<main class="main-content --tilt-tall">
  <header class="main-header --tilt">
    <h1 class="__title">Welcome to Snowdrift.coop!</h1>
  </header>

  <section class="auth-box --form">
    <p class="__intro">
      By signing up, you agree to our <a class="__link" href="/terms">Terms of
      Use</a> and <a class="__link" href="/privacy">Privacy Policy</a>.
    </p>
    <form class="signup-form" method="post"
    enctype="application/x-www-form-urlencoded">
      <input id="hident2" name="f1" class="__field --email" type="email"
      placeholder="email" required autofocus>
      <label class="__label --email" for="hident2">email</label>
      <div class="__error --email">Valid email address required.</div>
      <input id="hident3" name="f2" class="__field --passphrase"
      placeholder="passphrase" type="password" required minlength="9">
      <label class="__label --passphrase" for="hident2">passphrase</label>
      <div class="__error --passphrase">For security, we require your
      passphrase to have at least 9 characters.</div>
      <button class="__button" type="submit">Sign Up</button>
    </form>
  </section>

  <section class="auth-box --login">
      Already have an account? <a class="__link" href="/auth/login">Log in!</a>
  </section>
</main>
