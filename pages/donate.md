---
title: Donate
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Donate</h1>
  </header>

  <section class="main-section --intro">
    <p class="__text">
    Our goal is for <a class="__link" href="https://snowdrift.coop/p/snowdrift"
    >crowdmatching for Snowdrift.coop</a> eventually to meet all our funding
    needs. In our current startup phase, direct donations will help us reach
    that goal sooner, and are greatly appreciated.
    </p>
  </section>

  <section class="main-section --payment">
    <h2 class="__heading">Paypal</h2>
    <div class="donate-option --paypal" id="paypal-donate">
      <form class="__top" abframeid="iframe.0.4323007988200692"
      abineguid="E620BEBBFF4F4B1695F2E9204A329253"
      action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input name="cmd" type="hidden" value="_s-xclick">
        <input name="hosted_button_id" type="hidden" value="M24Z4DFEW4SQL">
        <input alt="PayPal donation option" name="submit"
        src="/img/donate/paypal.png" type="image">
      </form>
      <input id="toggle--paypal" class="__toggle" type="checkbox">
      <label for="toggle--paypal" class="__label">Fee-free option</label>
      <div class="__desc">
        The convenient button above works with or without a Paypal account, but
        it costs us a 2.9% + $0.30 fee. To skip the fee, log-in to Paypal and
        send funds to admin@snowdrift.coop funded by your bank or Paypal
        balance (not by credit card or paypal credit) and marked as a gift.
      </div>
    </div>
  </section>

  <section class="main-section --payment">
    <h2 class="__heading">Bitcoin</h2>
    <div class="donate-option --btc">
      <div class="__top">
        <img class="__btc" src="/img/donate/bitcoin.png"
        alt="Bitcoin logo, a B with lines at the top and bottom, like $">
        <p class="__address">1Bty8tCJDYGK2f5v5g71oLfL2qPTNvsDmB</p>
      </div>
      <input id="toggle--btc" class="__toggle" type="checkbox">
      <label for="toggle--btc" class="__label">Signature and options</label>
      <div class="__desc --pgp">
        <p>
          -----BEGIN PGP SIGNED MESSAGE-----<br>
          Hash: SHA256
        </p><p>
          Please use this address to publicly contribute to Snowdrift.coop:<br>
          1Bty8tCJDYGK2f5v5g71oLfL2qPTNvsDmB
        </p><p>
          We prefer you use the above address to help us with transparency, but if
          you would like us to generate an address specifically for your payment
          we will do so on request.
        </p><p>
          If you would like to be acknowledged or receive rewards, send us a
          message identifying yourself signed with the address you used to pay.
        </p><p>
          -----BEGIN PGP SIGNATURE-----<br>
          Version: GnuPG v1
        </p><p>
          iF4EAREIAAYFAlSOQskACgkQuHM2ODJ01/L2fQEApnLrKteMemNbVBFpsfbKu+Ea
          NYxSl4UfcZw8V0U5gwQBAJspPE1wRz89ed9RtsC11osgZdD47NZrhCAHR2PzlMD/
          =lB8a<br>
          -----END PGP SIGNATURE-----
        </p>
      </div>
    </div>
  </section>

  <section class="main-section --bonuses">
    <h2 class="__heading">Thank-you bonus options (on request)</h2>
    <dl class="__table">
      <dt class="__amount">$12+</dt>
      <dd class="__option">
        get a <a class="__link" href="/merchandise#stickers">sticker</a>
      </dd>
      <dt class="__amount">$25+</dt>
      <dd class="__option">
        get a $5 gift card to the working system
      </dd>
      <dt class="__amount">$40+</dt>
      <dd class="__option">
        be honored as a commit sponsor in the code
      </dd>
      <dt class="__amount">$100+</dt>
      <dd class="__option">
        get a <a class="__link" href="/merchandise#t-shirts">t-shirt</a>
      </dd>
      <dt class="__amount">$500+</dt>
      <dd class="__option">
        we will list you as a <a class="__link"
        href="https://snowdrift.coop/sponsors">key sponsor</a>
      </dd>
    </dl>
  </section>

  <section class="main-section --tax">
    <h2 class="__heading">Tax-deductible donations under 501(c)(3)</h2>
    <p class="__text">
      If making your donation tax-deductible under 501(c)(3) is important to you,
      please donate via our fiscal sponsor, the <a class="__link"
      href="https://opensource.org/civicrm/contribute/transact?reset=1&id=2">Open
      Source Initiative</a>, specify "Snowdrift.coop" in the Targeted Donations
      section, and <a class="__link" href="/contact">let us know</a>.
    </p>
  </section>
</main>
