# README

Prototype for [Snowdrift.coop] generated with [PieCrust].


## Requirements

- Python 3
- PieCrust
- Ruby/Bootstrap Sass


## Build

- Install ruby-sass from your distribution repos, e.g. in Debian derivatives,
  run `apt install ruby-sass`. Verify sass is available from the command line
  by running `sass -v`.

- Get project files: `git clone https://gitlab.com/sdproto/sdproto.gitlab.io`

- Change into the project directory and run: `make install`

- Build the prototype and start the preview server: `make`


## Contributing

See the [Contributor Guide].


## License

- HTML: [AGPLv3+]
- Assets: [CC-BY-SA]


## Credits

- Fonts and select icons: [Nunito] by Vernon Adams, [Fork Awesome]
- Characters: adaptations of [Mimi & Eunice] by Nina Paley


[Snowdrift.coop]: https://snowdrift.coop/
[PieCrust]: https://bolt80.com/piecrust/
[Contributor Guide]: CONTRIBUTING.md
[AGPLv3+]: https://www.gnu.org/licenses/agpl/
[CC-BY-SA]: https://creativecommons.org/licenses/by-sa/4.0/
[Nunito]: https://www.fontsquirrel.com/fonts/nunito
[Fork Awesome]: https://forkaweso.me/Fork-Awesome/
[Mimi & Eunice]: https://wiki.snowdrift.coop/communications/mimi-eunice
