---
title: Blender
---

<main class="main-content">
  <header class="project-masthead">
    <img class="__logo" src="/img/project/project-logo.png" alt="Blender">
  </header>

  <section class="project-intro">
    <p class="__desc">
      Free &amp; Open Source 3D creation. Free to use for any purpose, forever.
    </p>
    <video class="__video" controls poster="/img/project/project-video-poster.png">
     Your browser does not support the video tag.
    </video>
  </section>

  <section class="pledge-area">
    <a class="__button" href="#">Become a patron</a>
    <a class="__link" href="/how-it-works">how it works</a>

    <div class="pledge-illustration">
      <dl class="__patrons1">
        <dd class="__value">1415</dd>
        <dt class="__prop">patrons</dt>
      </dl>
      <dl class="__patrons2">
        <dd class="__value">141<span class="--change">6</span></dd>
        <dt class="__prop">patrons</dt>
      </dl>
      <dl class="__dollars1">
        <dd class="__value">1.415</dd>
        <dt class="__prop">dollars</dt>
      </dl>
      <dl class="__dollars2">
        <dd class="__value">1.41<span class="--change">6</span></dt>
        <dt class="__prop">dollars</dt>
      </dl>
      <div class="__arrow"></div>
      <dl class="__month1">
        <dd class="__value">2002</dd>
        <dt class="__prop">$/month</dt>
      </dl>
      <dl class="__month2">
        <dd class="__value">200<span class="--change">5</span></dd>
        <dt class="__prop">$/month</dt>
      </dl>
      <img class="__bubble" src="/img/project/bubble.svg" alt="If more join –
      we'll all donate more!">
    </div>
  </section>

  <section class="menu-tabs">
    <div class="__bg">
      <a class="__link" href="/project#about">About</a>
    </div>
    <div class="__bg">
      <a class="__link" href="/project#updates">Updates</a>
    </div>
  </section>

  <section class="project-info --about" id="about">
    <h2 class="__heading">About Blender</h2>
    <p class="__content">
      Blender is the free and open source 3D creation suite. It supports the
      entirety of the 3D pipeline — modeling, rigging, animation, simulation,
      rendering, compositing and motion tracking, even video editing and game
      creation.
    </p>

    <dl class="__content --summary">
      <dt class="__prop">homepage</dt>
      <dd class="__value"><a class="__link" href="#">blender.org</a></dd>
      <dt class="__prop">forum</dt>
      <dd class="__value"><a class="__link" href="#">devtalk.blender.org</a></dd>
      <dt class="__prop">license</dt>
      <dd class="__value"><a class="__link" href="#">GPL</a></dd>
      <dt class="__prop">mastodon</dt>
      <dd class="__value"><a class="__link" href="#">foo@bar.com</a></dd>
    </dl>

    <h3 class="__subheading">Get involved</h3>
    <p class="__content">
      Blender is a public project, made by hundreds of people from around the
      world; by studios and individual artists, professionals and hobbyists,
      scientists, students, VFX experts, animators, game artists, modders, and
      the list goes on.
    </p>
  </section>

  <hr class="__hr">

  <section class="project-info --updates" id="updates">
    <h2 class="__heading">Updates</h2>

    <article class="update-article">
      <time class="__timestamp" datetime="2020-02-14 20:00">2020-02-14</time>
      <h3 class="__subheading">
        <a class="__link" href="#">Blender 2.80-beta</a>
      </h3>
      <p class="__content">
        Read the release log, get the latest build and test files.
      </p>
      <img class="__content __thumb" src="/img/project/update1.jpg">
      <a class="__link --more" href="#">read more</a>
    </article>

    <article class="update-article">
      <time class="__timestamp" datetime="2020-02-10 20:00">2020-02-10</time>
      <h3 class="__subheading">
        <a class="__link" href="#">Blender Conference Early Bird</a>
      </h3>
      <p class="__content">
        Get a huge discount on the ticket for the best event of the year!
      </p>
      <img class="__content __thumb" src="/img/project/update2.jpg">
      <a class="__link --more" href="#">read more</a>
    </article>

    <article class="update-article">
      <time class="__timestamp" datetime="2020-01-20 20:00">2020-01-20</time>
      <h3 class="__subheading">
        <a class="__link" href="#">Blender wins Ub Iwerks Annie Award</a>
      </h3>
      <p class="__content">
        The animation industry's most prestigious technical award has been
        granted to Blender and Ton Roosendaal.
      </p>
      <img class="__content __thumb" src="/img/project/update3.jpg">
      <a class="__link --more" href="#">read more</a>
    </article>

    <p class="__note">there are 5 more…</p>
    <a class="__link --all" href="#">See all updates ></a>
  </section>
</main>
