---
title: Notifications
---
<!-- This page is under development. Do not export to production. -->
<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Notifications</h1>
  </header>

  <section class="intro-text">
    Here you can view site notifications.
  </section>

  <form class="alerts-delete" method="post"
  enctype="application/x-www-form-urlencoded">
    <a class="__button" href="#">Delete all notifications</a>
  </form>

  {# This is an example with duplicated filler data to show the list layout. #}
  {% for a in range(user.alerts.count) %}
    <input class="alert-close --dashboard" type="checkbox">
    <input class="alert-expand --dashboard" type="radio">
    <div class="alert-box --dashboard --info" >
      <h2 class="__title">Info notification</h2>
      <div class="__body">
        <p class="__msg">
          This is an informational notification.
        </p>
        <div class="__img --me"></div>
      </div>
    </div>
  {% endfor %}
</main>
