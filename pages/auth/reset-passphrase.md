---
title: Reset Passphrase
---

<main class="main-content">
  <header class="main-header --logo-small">
    <h1 class="__title">Reset passphrase</h1>
  </header>

  <section class="auth-box --form">
    <p class="__intro">To reset your Snowdrift.coop passphrase, enter your
    email and your NEW passphrase. We'll email you a security token required to
    complete the reset.</p>
    <form class="reset-form" method="post"
    enctype="application/x-www-form-urlencoded">
      <input id="hident2" name="f1" class="__field --email" type="email"
      placeholder="email" required autofocus>
      <label class="__label --email" for="hident2">email</label>
      <input id="hident3" name="f2" class="__field --passphrase"
      placeholder="passphrase" type="password" required minlength="9">
      <label class="__label --passphrase" for="hident2">new passphrase</label>
      <button class="__button" type="submit">Reset</button>
    </form>
    <div class="__img"></div>
  </section>
</main>
