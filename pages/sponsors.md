---
title: Sponsors
---

<main class="main-content --tilt">
  <header class="main-header --tilt">
    <h1 class="__title">Launch Sponsors</h1>
  </header>

  <p class="intro-text">
    We especially thank the following sponsors for their generous support of
    Snowdrift.coop initial development. Most donated during our 2014 <a
    class="__link" href="https://snowdrift.tilt.com/">fund-drive</a>, but we
    honor here all donations prior to our launch of the full crowdmatching
    system. <strong class="--strong">See our <a class="__link"
    ref="https://snowdrift.coop/donate">donation page</a> to become a launch
    sponsor and join the list below.</strong>
  </p>

  <h2 class="section-heading">Key sponsors</h2>
  <ul class="key-sponsors --col1">
    <li class="__sponsor">
      <a class="__link" href="https://www.alephobjects.com/">
        Aleph Objects, Inc.
      </a>
    </li>
    <li class="__sponsor">Denver Bohling</li>
    <li class="__sponsor">Philip Horger</li>
    <li class="__sponsor">jamessan</li>
    <li class="__sponsor">Tanu Kaskinen</li>
    <li class="__sponsor">
      <a class="__link" href="http://linuxfund.org/">Linux Fund</a>
    </li>
    <li class="__sponsor">Pat McGee</li>
  </ul>
  <ul class="key-sponsors --col2">
    <li class="__sponsor">Tomasz Muras</li>
    <li class="__sponsor">
      <a class="__link" href="http://pwgd.org/">People Who Give a Damn</a>
    </li>
    <li class="__sponsor">Paul Phillips</li>
    <li class="__sponsor">Kate & Ira Pohl</li>
    <li class="__sponsor">Johannes Wünsch</li>
    <li class="__sponsor">Michael Wünsch</li>
  </ul>

  <h2 class="section-heading">General pre-launch donors</h2>
  <ul class="general-sponsors --col1">
    <li class="__sponsor">3DInsider.com</li>
    <li class="__sponsor">Mateusz A.</li>
    <li class="__sponsor">Carlos Aguayo</li>
    <li class="__sponsor">Blaise Alleyne</li>
    <li class="__sponsor">Chris Aniszczyk</li>
    <li class="__sponsor">Jorge Aranda</li>
    <li class="__sponsor">Pierre Arlais, Bearstech</li>
    <li class="__sponsor">Christopher Armstrong</li>
    <li class="__sponsor">Xavier Antoviaque, OpenCraft</li>
    <li class="__sponsor">Artyom</li>
    <li class="__sponsor">Greg Austic</li>
    <li class="__sponsor">Autious</li>
    <li class="__sponsor">Kenneth Ballenegger</li>
    <li class="__sponsor">Moritz Bartl</li>
    <li class="__sponsor">Brennen Bearnes</li>
    <li class="__sponsor">Marcis Berzins</li>
    <li class="__sponsor">Michael Baldry</li>
    <li class="__sponsor">Nick Barry</li>
    <li class="__sponsor">Ingo Blechschmidt</li>
    <li class="__sponsor">Ludvig Böklin</li>
    <li class="__sponsor">Eduard Bopp</li>
    <li class="__sponsor">Nathan Bouscal</li>
    <li class="__sponsor">Jeremy Bowers</li>
    <li class="__sponsor">Bret Comnes</li>
    <li class="__sponsor">Tim Bunce</li>
    <li class="__sponsor">Brit Butler</li>
    <li class="__sponsor">Lee Butts</li>
    <li class="__sponsor">Javier Merino Cacho</li>
    <li class="__sponsor">Scott Calvert & family</li>
    <li class="__sponsor">Mike Chaberski</li>
    <li class="__sponsor">Jacob Chapman</li>
    <li class="__sponsor">Chato</li>
    <li class="__sponsor">Paul Chiusano</li>
    <li class="__sponsor">Jesper Cockx</li>
    <li class="__sponsor">CommonKarma.org</li>
    <li class="__sponsor">Antoine Comte</li>
    <li class="__sponsor">Noah Cornwell</li>
    <li class="__sponsor">Christopher Cummins</li>
    <li class="__sponsor">Daniel</li>
    <li class="__sponsor">Debian KDE</li>
    <li class="__sponsor">Jeroen Dekkers</li>
    <li class="__sponsor">Holger Dell</li>
    <li class="__sponsor">Sebastian Dröge</li>
    <li class="__sponsor">
      <a class="__link" href="http://andrewdurham.com/">Andrew Durham</a>
    </li>
    <li class="__sponsor">Christoph Egger</li>
    <li class="__sponsor">Carol Ann Emerick</li>
    <li class="__sponsor">Victor Engmark</li>
    <li class="__sponsor">Eric Entzel</li>
    <li class="__sponsor">Kent Fenwick</li>
    <li class="__sponsor">John Feras</li>
    <li class="__sponsor">Karl Fogel / QuestionCopyright.org</li>
    <li class="__sponsor">
      <a class="__link" href="http://fossetcon.org/">Fossetcon</a>
    </li>
    <li class="__sponsor">Christian Frank</li>
    <li class="__sponsor">Craig Fratrik</li>
    <li class="__sponsor">CrowdsourcingKC</li>
    <li class="__sponsor">
      <a class="__link" href="https://thefnf.org/">
        The Free Network Foundation
      </a>
    </li>
    <li class="__sponsor">freespace</li>
    <li class="__sponsor">FTL Software</li>
    <li class="__sponsor">Curtis Gagliardi</li>
    <li class="__sponsor">Luis Gasca</li>
    <li class="__sponsor">Denver Gingerich</li>
    <li class="__sponsor">Daniel Glassey</li>
    <li class="__sponsor">Brook Heisler</li>
    <li class="__sponsor">Raphaël Hertzog</li>
    <li class="__sponsor">Joey Hess</li>
    <li class="__sponsor">Jean-Baptiste Hétier</li>
    <li class="__sponsor">Joshua Hoblitt</li>
    <li class="__sponsor">Claudio Hoffmann</li>
    <li class="__sponsor">Thomas Hochstein</li>
    <li class="__sponsor">Martin Höcker</li>
    <li class="__sponsor">holdenweb</li>
    <li class="__sponsor">Lance Holton</li>
    <li class="__sponsor">hotzeplotz</li>
    <li class="__sponsor">Antonin Houska</li>
    <li class="__sponsor">Karl Ove Hufthammer</li>
    <li class="__sponsor">Eskild Hustvedt</li>
    <li class="__sponsor">Iko</li>
    <li class="__sponsor">Arya Irani</li>
    <li class="__sponsor">Geoffrey Irving</li>
    <li class="__sponsor">Seth de l'Isle</li>
    <li class="__sponsor">Marc Jeanmougin</li>
    <li class="__sponsor">Ethan Johnson</li>
    <li class="__sponsor">H. Ryan Jones</li>
    <li class="__sponsor">Ollie Jones</li>
    <li class="__sponsor">Michiel de Jong</li>
    <li class="__sponsor">Hasen el Judy | حسن الجودي | ハセン</li>
    <li class="__sponsor">Alan Keefer</li>
    <li class="__sponsor">Ian Kelling</li>
    <li class="__sponsor">Andrew Klofas</li>
    <li class="__sponsor">Robert Klotzner</li>
    <li class="__sponsor">Fabrice Knevez</li>
    <li class="__sponsor">Georg Kolling</li>
    <li class="__sponsor">Eric Kow</li>
    <li class="__sponsor">Alanna Krause</li>
    <li class="__sponsor">Ramana Kumar</li>
    <li class="__sponsor">Michael F. Lamb</li>
    <li class="__sponsor">Daniel Landau</li>
    <li class="__sponsor">Brianna Laugher</li>
    <li class="__sponsor">Jean-Pierre Laurin</li>
    <li class="__sponsor">Alexander Lang</li>
    <li class="__sponsor">Kirsten Larsen</li>
    <li class="__sponsor">Matt Lee</li>
    <li class="__sponsor">Nathan Lee</li>
    <li class="__sponsor">Randall Leeds</li>
    <li class="__sponsor">Charles Lehner</li>
    <li class="__sponsor">R. Diaz de Leon</li>
    <li class="__sponsor">Federico Leva</li>
    <li class="__sponsor">Aaron Levin</li>
    <li class="__sponsor">Carl Lewis</li>
    <li class="__sponsor">John A. Lewis</li>
    <li class="__sponsor">Katrin Leinweber</li>
  </ul>
  <ul class="general-sponsors --col2">
    <li class="__sponsor">Greg Lindahl</li>
    <li class="__sponsor">Thorbjørn Lindeijer</li>
    <li class="__sponsor">Mike Linksvayer</li>
    <li class="__sponsor">Yun-Mei Lo</li>
    <li class="__sponsor">Brendan Long</li>
    <li class="__sponsor">Hans Lub</li>
    <li class="__sponsor">Søren Lyhne</li>
    <li class="__sponsor">DJ Madeira</li>
    <li class="__sponsor">Magan Adam</li>
    <li class="__sponsor">Patrick Masson</li>
    <li class="__sponsor">Martin Mauch</li>
    <li class="__sponsor">Francois Marier</li>
    <li class="__sponsor">Sean McGregor</li>
    <li class="__sponsor">Ewen McNeill</li>
    <li class="__sponsor">Jan Mechtel</li>
    <li class="__sponsor">Simon Michael</li>
    <li class="__sponsor">Florin Mihaila</li>
    <li class="__sponsor">Matthew Miller (mattdm)</li>
    <li class="__sponsor">Scott Milliken</li>
    <li class="__sponsor">Havard Moen</li>
    <li class="__sponsor">Ramakrishnan Muthukrishnan</li>
    <li class="__sponsor">Lauro Gripa Neto</li>
    <li class="__sponsor">Perry Nguyen</li>
    <li class="__sponsor">Nmlgc</li>
    <li class="__sponsor">Lachlan O'Dea</li>
    <li class="__sponsor">Robert Orzanna</li>
    <li class="__sponsor">Étienne Vallette d'Osia</li>
    <li class="__sponsor">Tom Paluck</li>
    <li class="__sponsor">Răzvan Panda</li>
    <li class="__sponsor">Fabian Peters</li>
    <li class="__sponsor">Quinlan "The Cobra" Pfiffer</li>
    <li class="__sponsor">Karl Pietrzak</li>
    <li class="__sponsor">Steve Phillips / elimisteve</li>
    <li class="__sponsor">Pini</li>
    <li class="__sponsor">Josh Poimboeuf</li>
    <li class="__sponsor">Timotheus Pokorra</li>
    <li class="__sponsor">Michał Politowski</li>
    <li class="__sponsor">Nicholas Pontillo</li>
    <li class="__sponsor">Shuwen Qian</li>
    <li class="__sponsor">Aaron Quamme</li>
    <li class="__sponsor">Lane Rasberry</li>
    <li class="__sponsor">Olav Reinert</li>
    <li class="__sponsor">Dan Revel</li>
    <li class="__sponsor">Kevin Riggle</li>
    <li class="__sponsor">Matt Ritter</li>
    <li class="__sponsor">Florence Rosenbloom</li>
    <li class="__sponsor">Sage Ross</li>
    <li class="__sponsor">
      <a class="__link" href="http://www.roundware.org/">Roundware.org</a>
    </li>
    <li class="__sponsor">Hugo Roy (FSFE)</li>
    <li class="__sponsor">Noé Rubinstein</li>
    <li class="__sponsor">Michel S.</li>
    <li class="__sponsor">Marc Saegesser</li>
    <li class="__sponsor">Chris Sakkas, livinglibre.com</li>
    <li class="__sponsor">Olli Savolainen</li>
    <li class="__sponsor">Tobias Schachman</li>
    <li class="__sponsor">Brian Schroeder</li>
    <li class="__sponsor">Sean Seefried</li>
    <li class="__sponsor">Paul Sexton</li>
    <li class="__sponsor">Kendrick Shaw</li>
    <li class="__sponsor">Howard Lewis Ship</li>
    <li class="__sponsor">Juan Raphael Diaz Simões</li>
    <li class="__sponsor">Brandon Skari</li>
    <li class="__sponsor">Don Smith</li>
    <li class="__sponsor">Leon P Smith</li>
    <li class="__sponsor">Nick Smith</li>
    <li class="__sponsor">Jim Snow</li>
    <li class="__sponsor">Khaled Soliman</li>
    <li class="__sponsor">Rodrigo Souto</li>
    <li class="__sponsor">Adam Souzis</li>
    <li class="__sponsor">Adam Spitz</li>
    <li class="__sponsor">Charles Stanhope</li>
    <li class="__sponsor">Stephen Starkey</li>
    <li class="__sponsor">Startifact</li>
    <li class="__sponsor">statuszer0</li>
    <li class="__sponsor">Bob Steffes</li>
    <li class="__sponsor">Hugh Stimson</li>
    <li class="__sponsor">Morgan Stuart</li>
    <li class="__sponsor">Simon Tegg</li>
    <li class="__sponsor">Álvaro Tejero-Cantero</li>
    <li class="__sponsor">Zachary Tellman</li>
    <li class="__sponsor">Seth Tisue</li>
    <li class="__sponsor">Frank Thomas</li>
    <li class="__sponsor">Greg Tomei</li>
    <li class="__sponsor">Asbjørn Sloth Tønnesen</li>
    <li class="__sponsor">Kevin Turner</li>
    <li class="__sponsor">Christian Uhl</li>
    <li class="__sponsor">Vakranas</li>
    <li class="__sponsor">Timothy Vollmer</li>
    <li class="__sponsor">Wouter Vos</li>
    <li class="__sponsor">Markus Vuorio (Maakuth)</li>
    <li class="__sponsor">Tobias Gulbrandsen Waaler</li>
    <li class="__sponsor">Albert Wavering</li>
    <li class="__sponsor">Christopher Webber, GNU MediaGoblin</li>
    <li class="__sponsor">Stephen Paul Weber (singpolyma)</li>
    <li class="__sponsor">Patrick Weemeeuw</li>
    <li class="__sponsor">David Whitman</li>
    <li class="__sponsor">Kevin Wichmann</li>
    <li class="__sponsor">Alex Willemsma</li>
    <li class="__sponsor">Carl Witty</li>
    <li class="__sponsor">Barry & Devorah Wolf</li>
    <li class="__sponsor">Nicolas Wormser</li>
    <li class="__sponsor">Thomas Wrenn</li>
    <li class="__sponsor">Daniel Yokomizo</li>
    <li class="__sponsor">Brent Yorgey</li>
    <li class="__sponsor">Philip Young</li>
    <li class="__sponsor">Milan Zamazal</li>
    <li class="__sponsor">Shafiq Akram Zakaria</li>
    <li class="__sponsor">Massimo Zaniboni</li>
    <li class="__sponsor">Jan Zernisch</li>
    <li class="__sponsor">Jake Zieve</li>
    <li class="__sponsor">Guido Zuidhof</li>
  </ul>

  <div class="anon-sponsors">and 20 anonymous sponsors</div>
</main>
